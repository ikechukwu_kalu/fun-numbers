# FunNumbers

This is an Actions on Google project using an [Api.Ai](https://api.ai) agent. 

It consists of two parts: A Number Game and A Basic Calculator.

The ApiAi_Agents folder contains the Api.Ai agent which you can restore you project to.

You can extend the [Project](https://gitlab.com/ikechukwu_a/fun-numbers) to make something complex.

Feel free to send me a mail at [ikechukwuakalu@gmail.com](mailto:ikechukwuakalu@gmail.com) or follow me on [Twitter](https://twitter.com/ikechukwuakalu)